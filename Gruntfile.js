// WebSeal Generator by Tayfun Guelcan - mail@tayfunguelcan.de
// Generated on 2016-04-27 using generator-jade-rabbit 0.3.5
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        webseal: {
            // Configurable paths
            app: 'app',
            locales: 'locales',
            dist: 'dist',
            defaultLanguage: ''
        },

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            jade: {
                files: ['<%= webseal.app %>{,*/}/*.jade', '<%= webseal.app %>/**/{,*/}*.jade'],
                tasks: ['jade:server']
            },
            less: {
                files: ['<%= webseal.app %>/styles/{,*/}*.less'],
                tasks: ['less:server', 'autoprefixer']

            },
            js: {
                files: ['<%= webseal.app %>/scripts/{,*/}*.js'],
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            jstest: {
                files: ['test/spec/{,*/}*.js'],
                tasks: ['test:watch']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            styles: {
                files: ['<%= webseal.app %>/styles/{,*/}*.css'],
                tasks: ['newer:copy:styles', 'autopr2efixer']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '.tmp/{,*/}*.html',
                    '.tmp/styles/{,*/}*.css',
                    '<%= webseal.app %>/img/{,*/}*.{gif,jpeg,jpg,png,svg,webp}'
                ]
            }
        },

        // The actual grunt server settings
        connect: {
            options: {
                port: 9000,
                livereload: 35729,
                // Change this to '0.0.0.0' to access the server from outside
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    open: true,
                    base: [
                        '.tmp/<%= webseal.defaultLanguage %>',
                        '<%= webseal.app %>'
                    ]
                }
            },
            test: {
                options: {
                    port: 9001,
                    base: [
                        '.tmp',
                        'test',
                        '<%= webseal.app %>'
                    ]
                }
            },
            dist: {
                options: {
                    open: true,
                    base: '<%= webseal.dist %>',
                    livereload: false
                }
            }
        },

        wait: {
            options: {
                delay: 2000
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= webseal.dist %>/*',
                        '!<%= webseal.dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp',
            jade: {
                files: [{
                    dot: false,
                    src: [
                        '<%= webseal.app %>/index.html'
                    ]
                }]
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js',
                '<%= webseal.app %>/scripts/{,*/}*.js',
                '!<%= webseal.app %>/scripts/vendor/*',
                'test/spec/{,*/}*.js'
            ]
        },

        less: {
            options: {
                paths: ['<%= webseal.app %>/styles'],
                modifyVars: {
                    theme: '<%= theme %>'
                }
            },
            dist: {
                //options: {
                //    yuicompress: true,
                //    report: 'min' //gzip
                //},
                files: {
                    '.tmp/styles/main.css': '<%= webseal.app %>/styles/main.less'
                }
            },
            server: {
                //options: {
                //    dumpLineNumbers: 'all'
                //},
                files: {
                    '.tmp/styles/main.css': '<%= webseal.app %>/styles/main.less'
                }
            }
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    dest: '.tmp/styles/'
                }]
            }
        },

        // Renames files for browser caching purposes
        rev: {
            dist: {
                files: {
                    src: [
                        '<%= webseal.dist %>/scripts/{,*/}*.js',
                        '<%= webseal.dist %>/styles/{,*/}*.css',
                        '<%= webseal.dist %>/img/{,*/}*.{gif,jpeg,jpg,png,webp}',
                        '<%= webseal.dist %>/styles/fonts/{,*/}*.*'
                    ]
                }
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            options: {
                dest: '<%= webseal.dist %>'
            },
            html: '<%= webseal.app %>/index.html'
        },

        // Performs rewrites based on rev and the useminPrepare configuration
        usemin: {
            options: {
                assetsDirs: ['<%= webseal.dist %>']
            },
            html: ['.tmp/{,*/}*.html'],
            //css: ['<%= webseal.dist %>/styles/{,*/}*.css']
        },

        // The following *-min tasks produce minified files in the dist folder
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= webseal.app %>/img',
                    src: '{,*/}*.{gif,jpeg,jpg,png}',
                    dest: '<%= webseal.dist %>/img'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= webseal.app %>/img',
                    src: '{,*/}*.svg',
                    dest: '<%= webseal.dist %>/img'
                }]
            }
        },

        htmlmin: {
            dist: {
                options: {
                    removeCommentsFromCDATA: true,
                    removeEmptyAttributes: true,
                    removeRedundantAttributes: true,
                    useShortDoctype: true
                },
                files: [{
                    expand: true,
                    cwd: '.tmp',
                    src: '{,*/}*.html',
                    dest: '<%= webseal.dist %>'
                }]
            }
        },

        // By default, your `index.html`'s <!-- Usemin block --> will take care of
        // minification. These next options are pre-configured if you do not wish
        // to use the Usemin blocks.
        cssmin: {
            dist: {
                files: {
                    '<%= webseal.dist %>/styles/main.css': [
                        '.tmp/styles/{,*/}*.css',
                        '<%= webseal.app %>/styles/{,*/}*.css'
                    ]
                }
            }
        },
        // uglify: {
        //     dist: {
        //         files: {
        //             '<%= webseal.dist %>/scripts/scripts.js': [
        //                 '<%= webseal.dist %>/scripts/scripts.js'
        //             ]
        //         }
        //     }
        // },
        // concat: {
        //     dist: {}
        // },

        // Copies remaining files to places other tasks can use

        copy: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= webseal.app %>',
                    dest: '<%= webseal.dist %>',
                    src: [
                        '*.{ico,png,txt}',
                        '.htaccess',
                        'img/{,*/}*.webp',
                        '{,*/}*.html',
                        'fonts/{,*/}*.*',
                        'styles/{,*/}*.css'
                    ]
                }]
            },

            styles: {
                files: [{
                    expand: true,
                    cwd: '<%= webseal.app %>/styles',
                    dest: '.tmp/styles/',
                    src: '{,*/}*.css'
                }]
            }
        },

        // Run some tasks in parallel to speed up build process
        concurrent: {
            server: [
                'less:server',
                'copy:styles'
            ],
            test: [
                'copy:styles'
            ],
            dist: [
                'less:dist',
                'copy:styles',
                'imagemin',
                'svgmin'
            ]
        },

        jade: {
            server: {
                options: {
                    // jade i18n specific options
                    //i18n: {
                    //    locales: '<%= webseal.locales %>/*.json',
                    //    namespace: '$i18n',
                    //    localeExtension: false
                    //},
                    pretty: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= webseal.app %>',
                    dest: '.tmp',
                    src: '*.jade',
                    ext: '.html'
                }]
            },

            dist: {
                options: {
                    pretty: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= webseal.app %>',
                    dest: '<%= webseal.app %>',
                    src: 'index.jade',
                    ext: '.html'
                }]
            }
        }
    });

    grunt.registerTask('serve', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'connect:dist:keepalive']);
        }

        grunt.task.run([
            'clean:server',
            'concurrent:server',
            'jade:server',
            'connect:livereload',
            'watch'
        ]);
    });

    grunt.registerTask('server', function () {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run(['serve']);
    });

    grunt.registerTask('test', function (target) {
        if (target !== 'watch') {
            grunt.task.run([
                'clean:server',
                'concurrent:test',
                'autoprefixer'
            ]);
        }

        grunt.task.run([
            'connect:test'
        ]);
    });

    grunt.registerTask('build', [
        'clean:dist',
        'jade',
        'useminPrepare',
        'clean:jade',
        'wait',
        'concurrent:dist',
        //'autoprefixer',
        //'concat',
        'cssmin',
        //'uglify',
        'copy:dist',
        'copy:styles',
        //'rev',
        'usemin',
        'htmlmin'
    ]);

    grunt.registerTask('default', [
        'newer:jshint',
        'test',
        'build'
    ]);
};
